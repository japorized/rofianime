import { ensureFileSync, path } from "../deps.ts";
import { type Config } from "./config.ts";

export const BOOKMARK_FILE_NAME = ".bookmark";

export function getBookmarkValue(config: Config, showTitle: string): string {
  const bookmarkFile = path.join(
    config.show_directory,
    showTitle,
    BOOKMARK_FILE_NAME,
  );
  ensureFileSync(bookmarkFile);
  const bookmark = Deno.readTextFileSync(bookmarkFile).replace(/\n$/, "");

  return bookmark;
}

export function setBookmarkValue(
  config: Config,
  showTitle: string,
  epNumber: string,
): void {
  const bookmarkFile = path.join(
    config.show_directory,
    showTitle,
    BOOKMARK_FILE_NAME,
  );
  Deno.writeFileSync(
    bookmarkFile,
    new TextEncoder().encode(epNumber),
    { create: true, append: false },
  );
}
