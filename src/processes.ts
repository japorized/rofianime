import { Config } from "./config.ts";

export type CommandResponse<S, E> = [ProcessStatus, S, E];

export type ProcessStatus = {
  success: boolean;
  signal: Deno.Signal | null;
  code: number;
};

const commandOutputToProcessStatus = (
  output: Deno.CommandOutput,
): ProcessStatus => ({
  success: output.success,
  signal: output.signal,
  code: output.code,
});

const commandOutputToResponse = (
  output: Deno.CommandOutput,
): CommandResponse<string, string> => [
  commandOutputToProcessStatus(output),
  decodeProcessResponseText(output.stdout),
  decodeProcessResponseText(output.stderr),
];

function decodeProcessResponseText(resp: Uint8Array): string {
  return new TextDecoder().decode(resp).replace(/\n$/, "");
}

export async function rofi(
  prompt: string,
  args: string[],
  optionsToShow: string,
): Promise<CommandResponse<string, string>> {
  const proc = new Deno.Command("rofi", {
    args: [
      "-dmenu",
      "-i",
      "-p",
      prompt ?? "Rofianime",
      ...args,
      "-sep",
      "|",
    ],
    stdout: "piped",
    stdin: "piped",
    stderr: "piped",
  }).spawn();
  const stdinWriter = proc.stdin.getWriter();
  await stdinWriter.write(new TextEncoder().encode(optionsToShow));
  stdinWriter.close();

  return commandOutputToResponse(await proc.output());
}

export async function confirm(
  question: string,
): Promise<CommandResponse<boolean, string>> {
  const [confirmStatus, confirmChoice, confirmError] = await rofi("", [
    "-mesg",
    question,
    "-disable-history",
    "-theme",
    "rofi-simplemenu.rasi",
    "-theme-str",
    "window { padding: 20px 40px; }",
    "-theme-str",
    "listview { columns: 2; lines: 1; }",
    "",
  ], "No|Yes");

  return [confirmStatus, confirmChoice === "Yes", confirmError];
}

export async function runProcess(
  cmd: string,
  cmdArgs: string[] | undefined = undefined,
  stdinText?: string,
  opts: Pick<Deno.CommandOptions, "stdout" | "stderr"> = {
    stdout: "piped",
    stderr: "piped",
  },
): Promise<CommandResponse<string, string>> {
  const proc = new Deno.Command(cmd, {
    args: cmdArgs,
    stdin: "piped",
    stdout: opts.stdout,
    stderr: opts.stderr,
  }).spawn();
  if (stdinText) {
    const stdinWriter = proc.stdin.getWriter();
    await stdinWriter.write(new TextEncoder().encode(stdinText));
    proc.stdin.close();
  }

  const output = await proc.output();

  const stdout = opts.stdout === "piped"
    ? decodeProcessResponseText(output.stdout)
    : "";
  const stderr = opts.stderr === "piped"
    ? decodeProcessResponseText(output.stderr)
    : "";
  const status = commandOutputToProcessStatus(output);

  return [status, stdout, stderr];
}

export async function notify(
  config: Config,
  title: string,
  ...messages: unknown[]
): Promise<void> {
  await runProcess("notify-send", [
    `${config.prompt} — ${title}`,
    messages.join(" "),
  ]);
}
