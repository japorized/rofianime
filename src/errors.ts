export class InvalidDirectory extends Error {
  constructor(dir_path: string) {
    super(`${dir_path} is not a valid directory`);
  }
}

export class RofiError extends Error {
  constructor(msg: string) {
    super(`Got error from rofi${msg ? `: ${msg}` : ""}`);
  }
}

export class NoSelection extends Error {
  constructor() {
    super("Nothing was selected");
  }
}

export class BadEpName extends Error {
  constructor(name: string) {
    super(`${name} does not conform to the specified ep name format`);
  }
}

export class EnvVarIsUndefined extends Error {
  constructor(env_var: string) {
    super(`$${env_var} is undefined`);
  }
}

export class ConfigFileNotFound extends Error {
  constructor() {
    super(
      "The rofianime/config.toml file is not found in any of the specified directories",
    );
  }
}
