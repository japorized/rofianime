import { path } from "../deps.ts";
import { type Config, resolveEnvsInPath } from "./config.ts";
import { confirm, notify, runProcess } from "./processes.ts";
import { getShowEpNumber, isPathOccupied } from "./files.ts";
import { BOOKMARK_FILE_NAME } from "./bookmarks.ts";

type FileMovingPlan = {
  src: string;
  dest: string;
};

enum FileConflictResolutionStrategy {
  Overwrite = "Overwrite",
  SetAnotherEpNumber = "Set another EP number",
}

export async function importVideo(config: Config): Promise<void> {
  const fileFilters = config.accepted_file_exts.map((ext) => `*.${ext}`).join(
    " ",
  );
  const [fileSelectionStatus, rawPathToImportFiles, fileSelectionError] =
    await runProcess("zenity", [
      "--file-selection",
      "--multiple",
      "--separator=^",
      "--filename",
      `${resolveEnvsInPath("$HOME/Downloads/")}/`,
      "--file-filter",
      fileFilters,
    ]);
  if (!fileSelectionStatus.success) {
    if (fileSelectionError) {
      console.error(fileSelectionError);
      Deno.exit(1);
    }
    await notify(config, "Importing cancelled", "");
    return;
  }
  const pathToImportFiles = rawPathToImportFiles.split("^");

  const [destDirSelectionStatus, destDirPath, destDirSelectionError] =
    await runProcess("zenity", [
      "--file-selection",
      "--directory",
      "--filename",
      `${config.show_directory}/`,
    ]);
  if (!destDirSelectionStatus.success) {
    if (destDirSelectionError) {
      console.error(destDirSelectionError);
      Deno.exit(1);
    }
    await notify(config, "Importing cancelled", "");
    return;
  }
  const showName = path.basename(destDirPath);

  const movingPlans: FileMovingPlan[] = [];
  for (const importFile of pathToImportFiles) {
    const movingPlan: FileMovingPlan = {
      src: importFile,
      dest: "",
    };
    const fileExt = path.extname(importFile);

    while (movingPlan.dest === "") {
      const [epNumberInput, epNumber, epNumberInputError] = await runProcess(
        "zenity",
        [
          "--entry",
          `--text=Select episode number for\n${path.basename(importFile)}`,
        ],
      );
      if (!epNumberInput.success) {
        if (epNumberInputError) {
          await notify(
            config,
            "Error reading given EP number",
            epNumberInputError,
          );
          Deno.exit(1);
        }
        if (epNumberInput.code === 1) {
          await notify(config, "Importing cancelled", "");
          return;
        }
        continue;
      }

      const destPath = path.join(
        config.show_directory,
        showName,
        `${showName} - [${epNumber}]${fileExt}`,
      );
      const destPathIsAvailable = await isPathOccupied(destPath);
      if (destPathIsAvailable) {
        // This will end the while loop
        movingPlan.dest = destPath;
      } else {
        const [
          conflictStratSelectionStatus,
          conflictStrat,
          conflictStratSelectionError,
        ] = await runProcess("zenity", [
          "--list",
          '--column="0"',
          "--hide-header",
          ...(Object.values(FileConflictResolutionStrategy) as string[]),
        ]);
        if (!conflictStratSelectionStatus.success) {
          if (conflictStratSelectionError) {
            await notify(
              config,
              "Couldn't pick a conflict resolution strategy",
              conflictStratSelectionError,
            );
          } else if (conflictStratSelectionStatus.code === 1) {
            await notify(config, "Cancelled import", "");
            return;
          }
          Deno.exit(1);
        }

        switch (conflictStrat as FileConflictResolutionStrategy) {
          case FileConflictResolutionStrategy.Overwrite:
            movingPlan.dest = destPath;
            break;
          case FileConflictResolutionStrategy.SetAnotherEpNumber:
            continue;
        }
      }
    }

    movingPlans.push(movingPlan);
  }

  for (const movingPlan of movingPlans) {
    Deno.renameSync(movingPlan.src, movingPlan.dest);
  }
}

export async function renameShow(
  config: Config,
  selectedShowTitle: string,
): Promise<void> {
  const [setNewNameInputStatus, newShowName, setNewNameInputError] =
    await runProcess("zenity", [
      "--entry",
      `--text=Rename "${selectedShowTitle}" to...`,
    ]);
  if (!setNewNameInputStatus.success) {
    if (setNewNameInputError) {
      await notify(config, "Failed renaming show", setNewNameInputError);
      Deno.exit(1);
    }
  }
  if (!newShowName) {
    await notify(config, "Renaming cancelled", "");
    return;
  }

  const newShowPath = path.join(config.show_directory, newShowName);
  const newShowPathIsAvailable = await isPathOccupied(newShowPath);
  if (!newShowPathIsAvailable) {
    await notify(config, "Renaming failed", `${newShowName} is already taken.`);
    return;
  }

  const showPath = path.join(config.show_directory, selectedShowTitle);
  for (const epDirEntry of Deno.readDirSync(showPath)) {
    if (epDirEntry.name === BOOKMARK_FILE_NAME) continue;
    const oldEpPath = path.join(showPath, epDirEntry.name);
    const epNumber = getShowEpNumber(epDirEntry.name);
    const fileExt = path.extname(oldEpPath);
    const newEpPath = path.join(
      showPath,
      `${newShowName} - [${epNumber}]${fileExt}`,
    );
    Deno.renameSync(oldEpPath, newEpPath);
  }

  Deno.renameSync(showPath, newShowPath);
}

export async function deleteShow(
  config: Config,
  selectedShowTitle: string,
): Promise<void> {
  const [confirmStatus, confirmChoice, confirmError] = await confirm(
    `Are you sure you want to delete "${selectedShowTitle}"?`,
  );
  if (confirmStatus.success && confirmError === "" && confirmChoice) {
    await Deno.remove(
      path.join(config.show_directory, selectedShowTitle),
      { recursive: true },
    );
    await notify(
      config,
      "Show deletion",
      `"${selectedShowTitle}" is successfully deleted`,
    );
  }
}
