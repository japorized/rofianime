import { BadEpName } from "./errors.ts";

export const isPathOccupied = async (path: string) =>
  await Deno.lstat(path).then((_) => false)
    .catch((err) => {
      if (err instanceof Deno.errors.NotFound) {
        return true;
      } else {
        throw err;
      }
    });

export function getShowEpNumber(epName: string): string {
  const epNum = epName.match(/.* \- \[(\d+)\]\.[A-Za-z0-9]*$/)?.[1];
  if (epNum === undefined) throw new BadEpName(epName);
  return epNum;
}
