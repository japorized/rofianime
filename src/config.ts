import { type ParsedPath, parseToml, path } from "../deps.ts";
import {
  ConfigFileNotFound,
  EnvVarIsUndefined,
  InvalidDirectory,
} from "./errors.ts";

export const CONFIG_FILE_PATHS = [
  "$XDG_CONFIG_HOME/rofianime/config.toml",
  "$HOME/.config/rofianime/config.toml",
];

export type Config = {
  show_directory: string;
  prompt: string;
  video_player: string;
  file_manager: string;
  accepted_file_exts: string[];
  highlight_color: string;
  keybinds: {
    showSelection: {
      import: string;
      rename: string;
      delete: string;
    };
    epSelection: {
      go_back: string;
      open_in_file_manager: string;
      change_bookmark: string;
    };
  };
};

export function resolveEnvsInPath(
  givenPath: string,
  options = { throwOnMissing: false },
): string {
  const p = path.parse(givenPath);
  const pClone: ParsedPath = JSON.parse(JSON.stringify(p));
  for (const match of p.dir.matchAll(/\$[A-Za-z0-9_]+/g)) {
    let envVal = Deno.env.get(match[0].replace(/^\$/, ""));
    if (envVal === undefined) {
      if (options.throwOnMissing) {
        throw new EnvVarIsUndefined(match[0]);
      } else {
        envVal = "";
      }
    }
    pClone.dir = pClone.dir.replace(match[0], envVal);
  }

  for (const match of p.base.matchAll(/\$[A-Za-z0-9_]+/g)) {
    let envVal = Deno.env.get(match[0].replace(/^\$/, ""));
    if (envVal === undefined) {
      if (options.throwOnMissing) {
        throw new EnvVarIsUndefined(match[0]);
      } else {
        envVal = "";
        pClone.name = ""; // this prevents path.format to assume that a base is still available
      }
    }
    pClone.base = pClone.base.replace(match[0], envVal);
  }

  return path.format(pClone);
}

export async function loadConfig(): Promise<Config> {
  const configFilePath = (() => {
    for (const configFilePath of CONFIG_FILE_PATHS) {
      try {
        return resolveEnvsInPath(configFilePath, { throwOnMissing: true });
      } catch (_) {
        // do nothing and just let the resolver fail
      }
    }
    // We'll reach this point if the above for-loop doesn't ever return
    throw new ConfigFileNotFound();
  })();
  const configFileContents = Deno.readTextFileSync(configFilePath);
  const config = parseToml(configFileContents) as Config;

  // Ensuring defaults for non-file-related configs
  config.prompt = config.prompt || "Rofianime";
  config.accepted_file_exts = config.accepted_file_exts ||
    ["mkv", "mp4", "webm"];
  config.highlight_color = config.highlight_color || "Aqua";
  config.keybinds.showSelection.import = config.keybinds.showSelection.import ||
    "Alt+i";
  config.keybinds.showSelection.rename = config.keybinds.showSelection.rename ||
    "Alt+r";
  config.keybinds.showSelection.delete = config.keybinds.showSelection.delete ||
    "Alt+d";
  config.keybinds.epSelection.go_back = config.keybinds.epSelection.go_back ||
    "Alt+c";
  config.keybinds.epSelection.open_in_file_manager =
    config.keybinds.epSelection.open_in_file_manager || "Alt+f";
  config.keybinds.epSelection.change_bookmark =
    config.keybinds.epSelection.change_bookmark || "Alt+b";

  // Handle configs that are paths
  config.show_directory = config.show_directory
    ? resolveEnvsInPath(config.show_directory)
    : resolveEnvsInPath("$HOME/Videos");
  config.video_player = config.video_player
    ? resolveEnvsInPath(config.video_player)
    : "/usr/bin/xdg-open";
  config.file_manager = config.file_manager
    ? resolveEnvsInPath(config.file_manager)
    : "/usr/bin/xdg-open";

  // Check if given config is sane
  const show_dir_lstat = await Deno.lstat(config.show_directory).catch(
    (err) => {
      if (err instanceof Deno.errors.NotFound) {
        console.error(`${config.show_directory} is not found`);
      } else {
        console.error(
          `Could not verify if ${config.show_directory} is a valid directory`,
        );
      }
      Deno.exit(1);
    },
  );
  if (!show_dir_lstat.isDirectory) {
    console.error("Given show directory path isn't a directory");
    throw new InvalidDirectory(config.show_directory);
  }

  return config;
}
