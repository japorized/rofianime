prog_name := "rofianime"
install_dir := "${INSTALL_DIR:-$HOME/.bin}"
default_config_file_path := "${XDG_CONFIG_HOME:-$HOME/.config}/rofianime/config.toml"

build_output_dir := "./builds"

clean:
  rm -drf {{ build_output_dir }}/

run:
  deno run --allow-env --allow-read --allow-write --allow-run ./main.ts

test:
  deno test --allow-env ./tests/*.ts

compile:
  deno compile --allow-env --allow-read --allow-write --allow-run \
    --output {{ build_output_dir }}/{{ prog_name }} ./main.ts

install: clean compile
  #!/usr/bin/env bash
  if [[ -f "{{ install_dir }}/{{ prog_name }}" ]]; then
    echo -n "{{ prog_name }} already exists in {{ install_dir }}. Continue? (y/N) "
    read -n 1 -e ans
    echo ""
    if [[ "${ans^}" != "Y" ]]; then
      echo "Exiting..."
      exit 0
    fi
  fi
  echo "Copying {{ build_output_dir }}/{{ prog_name }} to {{ install_dir }}/{{ prog_name }}"
  cp {{ build_output_dir }}/{{ prog_name }} {{ install_dir }}/{{ prog_name }}

  # check and copy example config to default config dir
  if [[ ! -f "{{ default_config_file_path }}" ]]; then
    echo "Copying default config file to {{ default_config_file_path }}"
    config_file_dirname="$(dirname "{{ default_config_file_path }}")"
    mkdir -p "$config_file_dirname"
    cp ./config.example.toml "{{ default_config_file_path }}"
  fi
