#!/usr/bin/env -S deno run --allow-run --allow-read --allow-env --allow-write --no-check
import { path } from "./deps.ts";
import { type Config, loadConfig } from "./src/config.ts";
import { NoSelection, RofiError } from "./src/errors.ts";
import { notify, rofi, runProcess } from "./src/processes.ts";
import {
  BOOKMARK_FILE_NAME,
  getBookmarkValue,
  setBookmarkValue,
} from "./src/bookmarks.ts";
import { getShowEpNumber } from "./src/files.ts";
import { deleteShow, importVideo, renameShow } from "./src/ops.ts";

function sortDirEntriesAlphabetically(
  a: Deno.DirEntry,
  b: Deno.DirEntry,
): -1 | 0 | 1 {
  if (a.name < b.name) return -1;
  if (a.name > b.name) return 1;
  return 0;
}

function prettyPrintKeybind(
  config: Config,
  key: string,
  desc: string,
): string {
  return `<span color='${config.highlight_color}'>${key}</span> ${desc}`;
}

async function pickShow(config: Config): Promise<string> {
  const dirEntries = Array.from(Deno.readDirSync(config.show_directory)).filter(
    (dirEntry) => dirEntry.isDirectory,
  ).sort(sortDirEntriesAlphabetically);
  const showTitles = dirEntries.map((dirEntry) => dirEntry.name);
  const showSelection = await rofi(
    config.prompt,
    [
      "-no-custom",
      "-sep",
      "#",
      "-mesg",
      `${
        prettyPrintKeybind(
          config,
          config.keybinds.showSelection.import,
          "Import video",
        )
      } | ${
        prettyPrintKeybind(
          config,
          config.keybinds.showSelection.rename,
          "Rename show",
        )
      } | ${
        prettyPrintKeybind(
          config,
          config.keybinds.showSelection.delete,
          "Delete show",
        )
      }`,
      "-kb-custom-1",
      config.keybinds.showSelection.import,
      "-kb-custom-2",
      config.keybinds.showSelection.rename,
      "-kb-custom-3",
      config.keybinds.showSelection.delete,
    ],
    showTitles.join("#"),
  );
  const [returnStatus, selectedShowTitle, rofiErrorMsg] = showSelection;
  if (!returnStatus.success) {
    switch (returnStatus.code) {
      case 1:
        throw new NoSelection();
      case 10:
        await importVideo(config);
        return "";
      case 11:
        await renameShow(config, selectedShowTitle);
        return "";
      case 12:
        await deleteShow(config, selectedShowTitle);
        return "";
      default:
        throw new RofiError(rofiErrorMsg);
    }
  }

  return selectedShowTitle;
}

enum EpSelectionIntent {
  SelectEp,
  GoBack,
  SetBookmark,
}

async function pickEpisode(
  config: Config,
  selectedShow: string,
): Promise<[string, EpSelectionIntent]> {
  const showDirPath = path.join(config.show_directory, selectedShow);
  const currentBookmark = getBookmarkValue(config, selectedShow);
  const epDirEntries = Array.from(Deno.readDirSync(showDirPath))
    .filter((dirEntry) =>
      dirEntry.isFile && dirEntry.name !== BOOKMARK_FILE_NAME
    )
    .sort(sortDirEntriesAlphabetically);

  // Shift the next episode to top
  if (currentBookmark !== "") {
    const nextEpIdx = epDirEntries.findIndex((dirEntry) =>
      Number(getShowEpNumber(dirEntry.name)) - Number(currentBookmark) === 1
    );
    if (nextEpIdx !== -1) {
      const [nextEp] = epDirEntries.splice(nextEpIdx, 1);
      epDirEntries.unshift(nextEp);
    }
  }

  const epNames = epDirEntries.map((dirEntry) => dirEntry.name);
  const epSelection = await rofi(
    config.prompt,
    [
      "-no-custom",
      "-sep",
      "^",
      "-mesg",
      `<b>Last Episode</b>: ${currentBookmark || "-"}
${
        prettyPrintKeybind(
          config,
          config.keybinds.epSelection.go_back,
          "Go back",
        )
      } | ${
        prettyPrintKeybind(
          config,
          config.keybinds.epSelection.open_in_file_manager,
          "Open in file manager",
        )
      }
${
        prettyPrintKeybind(
          config,
          config.keybinds.epSelection.change_bookmark,
          "Change bookmark",
        )
      }`,
      "-kb-custom-1",
      config.keybinds.epSelection.go_back,
      "-kb-custom-2",
      config.keybinds.epSelection.open_in_file_manager,
      "-kb-custom-3",
      config.keybinds.epSelection.change_bookmark,
    ],
    epNames.join("^"),
  );
  const [returnStatus, selectedEpName, rofiErrorMsg] = epSelection;
  if (!returnStatus.success) {
    switch (returnStatus.code) {
      case 1:
        throw new NoSelection();
      case 10:
        // We don't have to do anything here. Returning an empty string will
        // bring us back to the main menu.
        return ["", EpSelectionIntent.GoBack];
      case 11:
        await new Deno.Command(config.file_manager, {
          args: [showDirPath],
          stdin: "null",
          stdout: "null",
          stderr: "null",
        }).output();
        Deno.exit(0);
        // This never falls through cause of Deno.exit, but the linter isn't happy
        /* falls through */
      case 12: {
        const [
          setBookmarkValueStatus,
          newBookmarkValue,
          setBookmarkValueError,
        ] = await runProcess("zenity", [
          "--entry",
          `--text=Current bookmark: ${currentBookmark}`,
        ]);
        if (!setBookmarkValueStatus.success) {
          if (setBookmarkValueError) {
            await notify(
              config,
              "Failed setting boomark",
              setBookmarkValueError,
            );
            Deno.exit(1);
          }
        } else {
          setBookmarkValue(config, selectedShow, newBookmarkValue);
          await notify(
            config,
            "Bookmark update successful",
            `Changed bookmark from ${currentBookmark} to ${newBookmarkValue}`,
          );
        }
        return ["", EpSelectionIntent.SetBookmark];
      }
      default:
        throw new RofiError(rofiErrorMsg);
    }
  }

  return [selectedEpName, EpSelectionIntent.SelectEp];
}

async function playEpisode(config: Config, fullEpPath: string): Promise<void> {
  const [playerExitStatus] = await runProcess(
    config.video_player,
    [
      fullEpPath,
    ],
    undefined,
    { stdout: "null", stderr: "null" },
  );
  if (playerExitStatus.success) {
    const parsedPath = path.parse(fullEpPath);
    const epNumber = getShowEpNumber(parsedPath.base);
    setBookmarkValue(config, path.basename(parsedPath.dir), epNumber);
  } else {
    Deno.exit(1);
  }
}

async function main() {
  const config = await loadConfig();

  let selectedShow = "";
  let selectedEp = "";
  let epSelectionIntent: EpSelectionIntent;
  while (selectedEp === "") {
    // If the selected show is already chosen, skip to the next step
    if (selectedShow === "") {
      selectedShow = await pickShow(config).catch((err) => {
        if (err instanceof NoSelection) {
          Deno.exit(0);
        } else {
          console.error(err);
          Deno.exit(1);
        }
      });
    }
    // If a show isn't selected, continue with the loop to select a show
    if (selectedShow === "") continue;

    [selectedEp, epSelectionIntent] = await pickEpisode(config, selectedShow)
      .catch((err) => {
        if (err instanceof NoSelection) {
          Deno.exit(0);
        } else {
          console.error(err);
          Deno.exit(1);
        }
      });
    switch (epSelectionIntent) {
      case EpSelectionIntent.GoBack:
        selectedEp = "";
        selectedShow = "";
        break;
      default:
        break;
    }
  }

  const fullEpPath = path.join(config.show_directory, selectedShow, selectedEp);
  await playEpisode(config, fullEpPath);
}

await main();
