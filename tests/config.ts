import { assertEquals, assertThrows } from "../test_deps.ts";
import { resolveEnvsInPath } from "../src/config.ts";

Deno.test("resolveEnvsInPath resolves environment variables in a given path", () => {
  Deno.env.set("HOME", "/home/rofianime");
  Deno.env.set("ROFIANIME_DIR_NAME", "eminaifor");

  assertEquals(
    resolveEnvsInPath("$HOME/.config/$ROFIANIME_DIR_NAME"),
    "/home/rofianime/.config/eminaifor",
  );
});

Deno.test("resolveEnvsInPath optionally throws on missing env", () => {
  Deno.env.set("HOME", "/home/rofianime");
  assertEquals(
    resolveEnvsInPath("$HOME/.local/state/$SOME_NON_EXISTENT_ENV_VAR"),
    "/home/rofianime/.local/state/",
  );

  assertThrows(
    () => {
      resolveEnvsInPath("$HOME/.local/state/$SOME_NON_EXISTENT_ENV_VAR", {
        throwOnMissing: true,
      });
    },
  );
});
