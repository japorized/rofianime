# rofianime

A sufficient video viewing utility

---

## Installation

### Dependencies

- rofi (tested with v1.7.5)
- zenity (tested with v3.43.0)
- notify-send (tested with v0.8.1)
- (optional) mpv [default video player]

Build dependencies:

- deno (built with v1.27.2)
- just

If you have `nix` with `flake` enabled, just `nix develop -c $SHELL`.

### How to Install

```
git clone https://gitlab.com/japorized/rofianime
cd rofianime
just install
```

`rofianime` will be installed in `$HOME/.bin` by default. Change the `INSTALL_DIR` environment variable to install `rofianime` in a different location. E.g.

```
INSTALL_DIR=/path/to/installation/dir just install
```

---

## Usage

To use, run

```
rofianime
```

### Configuration

The default configuration file will be created in the first folder of which all
environment variables are defined:

- `$XDG_CONFIG_HOME/rofianime/config.toml`
- `$HOME/.config/rofianime/config.toml`

You can use environment variables in configurations that takes a path. Note,
that unset variables will evaluate to an empty string (`""`).

The syntax for keybinds follow that of `rofi`. See `man rofi-keys`.

---

## Features

- Keep track of last-viewed episode of a show (called a bookmark)
- Import video files into shows, and automatically rename them to a format
  `rofianime` understands
- Rename shows, which also renames the video files into the expected format

## Non-features

- Syncing bookmarks

---

## Usage Notes

- Video files are expected to be named in the following format:
  `<show name> - [<show number>].<file extension>`
